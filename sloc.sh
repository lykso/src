#!/bin/sh

# find all *.sh files not under `pkg` that are not symbolic links, strip all
# trailing whitespace, then all leading whitespace, then all lines starting
# with '#', then all empty lines. then count the remaining lines.

find . -name "*.sh" ! -path "**/pkg/**" ! -type l \
| xargs sed 's/[[:space:]]*$//g; s/^[[:space:]]*//g; s/^#.*$//g; /^$/d' \
| wc -l - \
| cut -d' ' -f1

# note that this script's ELOC is also included in the count.
