#!/bin/sh -e

export SRCROOT="$(dirname "$(readlink -f "$0")")"
export PREFIX="${PREFIX:-${0%/bin/src}}"
export SRCREPO="${SRCREPO:-$PREFIX/var/src}"

while [ "${1#-}" != "$1" ]; do
    case "$1" in
        -e|--exists) exists='yes' ;;
        -v|--verbose) verbose='yes' ;;
        -d|--defaults) defaults='yes' ;;
        -s|--set-checks) setchecks='yes' ;;
    esac
    shift
done

[ "$1" ] || { eval "echo \"$(cat $SRCROOT/USAGE)\"" && exit 0; }

. "$SRCROOT/lib/log.sh"
. "$SRCROOT/lib/get_file.sh"

[ -d "$SRCREPO" ] || err "$SRCREPO is not a directory!"

export pkgname="$1"

pkg="$SRCROOT/pkg/$pkgname"
[ -e "$pkg/checks" ] || [ "$setchecks" ] \
|| err "'checks' file missing for $pkgname!"

[ ! -e "$pkg/defaults.sh" ] || . "$pkg/defaults.sh" 
[ "$2" = "default" ] || version="${2:-$version}"
export version
export type="${3:-$type}"

if [ "$defaults" ]; then
    cat "$pkg/defaults.sh"
    exit 0
fi

[ "$version" ] && [ "$type" ] || err "type and version could not be determined."

# load the archive type's 'get_archive' function
. "$SRCROOT/lib/type/$type.sh"

arcdest="$SRCREPO/$pkgname/$version"
sigdest="$pkg/sig/$pkgname-$version.$type.sig"

if [ "$exists" ]; then
    [ -e "$arcdest" ] && exit 0 || exit 1
fi

[ ! -e "$arcdest" ] || err "$arcdest exists! skipping pull." \

# if the package is a symlink to a sibling directory, make a corresponding
# symlink in SRCREPO and bail to avoid duplicating source code.
 linktarget="$(readlink "$SRCROOT/pkg/$1" 2> /dev/null)" \
  && [ "${linktarget#*/}" = "$linktarget" ] \
  && ln -sf "$SRCREPO/$linktarget" "$SRCREPO/$pkgname" \
  && log "symlinked $SRCREPO/$pkgname to $SRCREPO/$linktarget." \
 || true # force evaluation to true since "sh -e" exits on non-zero exit codes.

# if a symlink was created, do the existence check again.  act like the code
# was just pulled if the symlink made the sought version available at $arcdest.
[ ! "$linktarget" ] \
|| [ ! -e "$arcdest" ] \
|| { echo "$arcdest" && exit 0; }

# otherwise, let's continue with the business of making the remote requests.
tryurls() {
    abbr="$(echo $1 | cut -c1-3)" # 'sig' or 'arc'
    dest="$2"
    urlsfile="$(eval "echo \"$(cat "$pkg/urls")\"")"
    urls="$(echo "$urlsfile" | awk "/^$version\t$type\t$abbr\t/")"
    [ -n "$urls" ] || return 0

    check="$(eval "echo \"$(awk "/$version\t$type\t$abbr\t/" "$pkg/checks")\"")"
    if [ ! "$check" ] && [ ! "$setchecks" ]; then
        err "no '$abbr' check for $pkgname $version."
    fi

    size="$(echo "$check" | cut -f4)"
    sum="$(echo "$check" | cut -f5)"

    log "grabbing $pkgname $version $type $1..."
    mkdir -p "$(dirname "$dest")"

    echo "$urls" | while read line; do
        url="$(echo "$line" | cut -f4)"
        log "trying $url"

        get_$3 "$dest" "$url" "$size" && break || continue
    done
    [ -e "$dest" ] || err "failed to get $pkgname $version $type $1!"

    log "calculating size and checksum..."
    truesize="$(${3}_size "$dest")"
    truesum="$(${3}_checksum "$dest")"
    
    if [ ! "$setchecks" ]; then
        [ "$truesize" = "$size" ] || err "expected size $size, got $truesize."
        [ "$truesum" = "$sum" ] || err "expected checksum $sum, got $truesum."
    else
        log "setting checks for $name $version"
        printf "$version\t$type\t$abbr\t$truesize\t$truesum\n" >> $pkg/checks
        [ ! "$check" ] || sed -i "/^$check\$/d" "$pkg/checks" || true
    fi
}

arcname="$(archive_name)"
tryurls "signature" "$sigdest" "file"
tryurls "archive" "$arcname" "archive"

if [ -f "$sigdest" ]; then
    if (which gpg 1>&2 2>/dev/null); then
        log "checking signature..."
        sigcheck_archive "$arcname" "$sigdest"
    else
        wrn "signature available but gpg not found! skipping 'sig' check."
    fi
fi

unpack_archive "$arcname" "$arcdest"

[ "$verbose" ] \
&& log "$pkgname $version source code extracted to $arcdest" \
|| echo "$arcdest"
