get_archive() {
    local gitdest="$1"
    local url="$2"

    # would be great if there were a way to restrict size here.
    if [ -d "$gitdest" ]; then
        (cd "$gitdest" && git fetch --all)
    else
        git clone --mirror "$url" "$gitdest"
    fi
}

archive_name() {
    echo "$SRCROOT/pkg/$pkgname/git"
}

archive_size() {
    # don't do this!  different sizes on different filesystem types.
    ##expr $(du -sk $1 | cut -f1) \* 1024
    
    cd "$1"
    git count-objects -vH | grep size-pack | cut -d': ' -f2
    cd - >/dev/null
}

archive_checksum() {
    cd "$1"
    find . -type f ! -path "**/.git/**" ! -name ".git" \
           | sort \
           | xargs sha512sum \
           | cut -d' ' -f1 \
           | sha512sum \
           | cut -d' ' -f1
    cd - >/dev/null
}

unpack_archive() {
    cd "$1"
    git worktree add -f "$2" "$version"
    cd - >/dev/null
}

sigcheck_archive() { return 0; }
