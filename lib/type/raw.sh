get_archive() {
    [ -d "$pkg/raw" ] || mkdir -p "$pkg/raw"

    get_file "$1" "$2" "$3" || return 1

    return 0
}

sigcheck_archive() {
    gpg --homedir="$SRCROOT/gpg" --verify "$2" "$1" \
    || err "$2 does not contain a valid signature for $1"
}

unpack_archive() {
    [ -d "$2" ] || mkdir -p "$2"
    mv "$1" "$2/$pkgname"
    rmdir "$pkg/raw" 2>/dev/null || true
}

archive_name() {
    echo "$pkg/raw/$pkgname-$version"
}

archive_size() {
    file_size $@
}

archive_checksum() {
    file_checksum $@
}
