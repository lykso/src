# for tarballs that use kernel.org's style of signature, where the release
# signature is made against the uncompressed tarball.
get_archive() {
    [ -d "$pkg/tarball" ] || mkdir -p "$pkg/tarball"

    local tarball="$1"
    local ext="$(echo "$type" | cut -d- -f2)"
    get_file "$tarball" "$url" || return 1

    log "decompressing tarball..."

    case "$ext" in
        xz) unxz -k "$tarball" ;;
        gz) gunzip -k "$tarball" ;;
        *) err "unrecognized extension: $ext" ;;
    esac

    return $?
}

sigcheck_archive() {
    local ext="$(echo "$type" | cut -d- -f2)"
    local tarball="${1%.$ext}"
    gpg --homedir="$SRCROOT/gpg" --verify "$2" "$tarball" \
    || err "$2 does not contain a valid signature for $tarball"
}

unpack_archive() {
    local ext="$(echo "$type" | cut -d- -f2)"
    local tarball="${1%.$ext}"
    local arcdest="$2"

    log "extracting tarball..."

    # extract the tarball into a temporary directory first, then move either it
    # or its contents, if it contained a single directory, to $pkgdest
    tempdir="$(mktemp -d)"
    tar -xf "$tarball" -C $tempdir
    contents="$(ls "$tempdir")"

    if [ "$(echo "$contents" | wc -l)" -gt 1 ]; then
        mkdir -p "$arcdest"
        contents="."
    else
        mkdir -p "$(dirname $arcdest)"
    fi
    mv "$tempdir/$contents" "$arcdest"
    rm -r "$tempdir"
    rm "$tarball" # remove the uncompressed tarball

    return 0
}

archive_name() {
    local ext="$(echo "$type" | cut -d- -f2)"
    echo "$pkg/tarball/$pkgname-$version.tar.$ext"
}

archive_size() {
    file_size $@
}

archive_checksum() {
    file_checksum $@
}
