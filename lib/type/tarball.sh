get_archive() {
    [ -d "$pkg/tarball" ] || mkdir -p "$pkg/tarball"

    get_file "$1" "$url" "$3"

    return $?
}

unpack_archive() {
    log "extracting tarball..."

    # extract the tarball into a temporary directory first, then move either it
    # or its contents, if it contained a single directory, to $pkgdest
    tempdir="$(mktemp -d)"
    tar -xf "$1" -C $tempdir
    contents="$(ls "$tempdir")"

    if [ "$(echo "$contents" | wc -l)" -gt 1 ]; then
        mkdir -p "$2"
        contents="."
    else
        mkdir -p "$(dirname "$2")"
    fi

    mv "$tempdir/$contents" "$2"
    rm -r "$tempdir"

    return 0
}

sigcheck_archive() {
    gpg --homedir="$SRCROOT/gpg" --verify "$2" "$1" \
    || err "$2 does not contain a valid signature for $1"
}

archive_name() {
    echo "$pkg/tarball/$pkgname-$version.$type"
}

archive_size() {
    file_size $@
}

archive_checksum() {
    file_checksum $@
}
