# the same logic applies whether downloading a tarball or a signature file.
# don't accept anything but the expected number of bytes and check the checksum.
get_file() {
    local dest="$1"
    local url="$2"
    local size="$3"

    # being explicit about the size of the file allows protection against DoS
    # attacks where the attacker sends an infinite stream of bytes.  it may also
    # make finding hash collisions against a particular hash impossible, since
    # it may be that no collision using an input equal to the given size exists.
    [ ! "$size" ] || sizeopt="-r 0-$size"
    curl $sizeopt --compressed -C - -L -o "$dest" "$url"

    return $?
}

file_size() {
    wc -c "$1" | cut -d' ' -f1
}

file_checksum() {
    sha512sum "$1" | cut -d' ' -f1
}

file_name() {
    echo "$1"
}
