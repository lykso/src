# if _colors is not already set, if this is running on an interactive
# terminal (as opposed to in a script), if tput is installed, and if tput
# knows some color codes, then set _colors to 'yes'.
{  [ -z "$_colors" ] \
&& [ -t 1 ] \
&& [ "$(tput colors 2>/dev/null || echo 0)" -ge 8 ] \
&& _colors="yes"; } || true # suppress error codes

_clr="$({ [ "$_colors" = 'yes' ] && tput sgr0; } || echo '')"
_blu="$({ [ "$_colors" = 'yes' ] && tput setaf 6; } || echo '')"
_ylw="$({ [ "$_colors" = 'yes' ] && tput setaf 3; } || echo '')"
_red="$({ [ "$_colors" = 'yes' ] && tput setaf 1; } || echo '')"

log() { [ "$verbose" != 'yes' ] || echo "$_blu[LOG]$_clr $@"; }
wrn() { echo "$_ylw[WRN]$_clr $@" >&2; }
err() { echo "$_red[ERR]$_clr $@" >&2; exit 1; }
