SRCDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
PREFIX ?= /usr
DESTDIR ?= $(PREFIX)/bin
SRCREPO ?= $(PREFIX)/var/src
SRCROOT ?= $(PREFIX)/share/src
GITBASE ?= $(shell realpath $(shell git config --get remote.origin.url) | rev \
           | cut -d/ -f2- | rev)

.PHONY: install uninstall lix-os-pkg

lix-os-pkgs:
	git clone $(GITBASE)/src-lix-os-pkgs pkg

install:
	mkdir -p $(SRCREPO)
	ln -sf $(SRCDIR) $(SRCROOT)
	ln -sf $(SRCROOT)/src.sh $(DESTDIR)/src

uninstall:
	rm $(DESTDIR)/src
	rm $(SRCROOT)
	rm -fr $(SRCREPO)
